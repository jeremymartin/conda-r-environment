#!/bin/bash

# Complete this variable with the path to CondaR directory
# ex: PATH_TO_CONDA_R_DIRECTORY="$HOME/bin/CondaR"
PATH_TO_CONDA_R_DIRECTORY=""

if [[ ! -z ${CONDA_PREFIX+x} && ${PATH_TO_CONDA_R_DIRECTORY} ]]
then
  echo "Copying activate.d/env_vars.sh into \"$CONDA_DEFAULT_ENV\" environment directory..."
  cp $PATH_TO_CONDA_R_DIRECTORY/activate.env_vars.sh $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh
  echo "     ...DONE!"
  echo "Copying deactivate.d/env_vars.sh into \"$CONDA_DEFAULT_ENV\" environment directory..."
  cp $PATH_TO_CONDA_R_DIRECTORY/deactivate.env_vars.sh $CONDA_PREFIX/etc/conda/deactivate.d/env_vars.sh
  echo "     ...DONE!"
else
  echo "There is not any conda environment activated or you haven't completed the PATH_TO_CONDA_R_DIRECTORY variable in conda-r.sh"
fi

unset PATH_TO_CONDA_R_DIRECTORY
