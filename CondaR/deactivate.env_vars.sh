#!/usr/bin/env sh

# unset library path
unset LD_LIBRARY_PATH

# restore old library path
if [ ! -z ${OLD_LD_LIBRARY_PATH+x} ]; then
  export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH
fi

unset OLD_LD_LIBRARY_PATH
