#!/usr/bin/env sh

# store old LD_LIBRARY_PATH:
if [ ! -z ${LD_LIBRARY_PATH+x} ]; then
  OLD_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
fi

# export Conda env libs
export LD_LIBRARY_PATH=$CONDA_PREFIX/lib:$LD_LIBRARY_PATH
