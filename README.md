# Conda R Environment

Theses scripts are my personnal solution to fix internet routines errors with local installation of RStudio while a Conda environment is activated.

The error occures when Conda environment is activate and when I try to work with RStudio on PopOS! or Ubuntu (18.04 and 20.04) :
```
Error in tools::startDynamicHelp() : 
    internet routines cannot be loaded
```

# How To Use

[Download the CondaR folder](https://gitlab.com/jeremymartin/conda-r-environment/-/archive/master/conda-r-environment-master.zip?path=CondaR), then complete the `PATH_TO_CONDA_R_DIRECTORY` from conda-r.sh as it's done in the exemple.

Launch the script when you are in an environment, then reload it :

```
conda activate my_env
./conda-r.sh
conda deactivate
conda activate my_env
```

If you have a `$HOME/bin` folder in your `$PATH`, you can put here the CondaR directory and create a symbolic link to CondaR/conda-r.sh

```sh
cd ~/bin
mv ~/Downloads/CondaR .
ln -s CondaR/conda-r.sh conda-r
```

Then, you just have to execute `conda-r` when you create a new environment where you want to use Conda R version with local RStudio.

In this case, don't forget to remplace in conda-r.sh :

`PATH_TO_CONDA_R_DIRECTORY="$HOME/bin/CondaR"`

# How To Remove

If unfortunately this dosen't fix the error for you, or if this cause other problem (like core dump), it's preferable to remove the env_vars.sh from activate.d and deactivate.d folder :

```bash
conda activate my_env
cd $CONDA_PREFIX/etc/conda
rm activate.d/env_vars.sh
rm deactivate.d/env_vars.sh
```

# More

This tool was written to fix a technical problem during my Msc in Bioinformatics. I have nothing to see with RStudio, R, or Anaconda and it's possible that theses scripts will never be updated.

I know this error can occures on MacOS or Windows systems, but I don't know if this solution can fix the problem, and I don't recommand you to try this if you're not sure about these manipulations.

I'm glad if this could help you
